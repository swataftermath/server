@ECHO OFF
echo Generating Root RSA key pair
ssh-keygen -f .docker/sshd/host_keys/id_rsa -N "" -t rsa -C "SwatAftermath.com" -b 4096
echo Generating Host RSA key pair
ssh-keygen -f .docker/sshd/host_keys/ssh_host_rsa_key -N "" -t rsa -C "SwatAftermath.com" -b 4096
echo Generating Host DSA key pair
ssh-keygen -f .docker/sshd/host_keys/ssh_host_dsa_key -N "" -t dsa -C "SwatAftermath.com" -b 1024
echo Generating Host ECDSA key pair
ssh-keygen -f .docker/sshd/host_keys/ssh_host_ecdsa_key -N "" -t ecdsa -C "SwatAftermath.com
echo Generating Host ED25519 key pair
ssh-keygen -f .docker/sshd/host_keys/ssh_host_ed25519_key -N "" -t ed25519 -C "SwatAftermath.com"
PAUSE