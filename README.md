# Swat Aftermath Server

A full build of all the public parts of SwatAftermath.com. Some integrations are protected, like code-logic, and reside elsewhere.

## [Server](https://gitlab.com/swataftermath/server)

[Virtualized and containerized with Docker](https://gitlab.com/swataftermath/server/-/blob/master/docker-compose.yml).

- Securely built with Ubuntu 20.04
- Provisioned with [cloud-config](https://gitlab.com/swataftermath/server/-/blob/master/cloud-config.yaml)
- Services and applications run separately
- All public content is served from `/public`, and scripts run from there too

### Technical specifications

Running on a Digital Ocean $5 /mo Droplet, or any equivalent service with at least 1 vCPU, 1 GB RAM, 1 GB SSD drive.

- 1 GB RAM / 1 vCPU
- 25 GB SSD Disk
- 1000 GB Transfer
- Hosted in San Francisco
- User Data add-on
  - See [cloud-config.yaml](/cloud-config.yaml)
- Backup add-on
- Randomized SSH-key

Entire stack is optimized to be open source and transparent, reducing unnecessary overhead and bloat from old applications.

### Design decisions

- [x] Load and render server-side
  - Server-side is a harder point of failure
  - Fetching requires building endpoints
  - Endpoints are the future, but requires more end-to-end testing
- [x] Single SQL-database for all data
  - Single is less redundant but more easily unified
  - Single requires more concurrency
  - Per-player is more robust but requires more endpoints
  - Per-player does not suffer from concurrency
  - Concurrency with current userbase is a low-level issue, and upgrading infrastructure is easy
  - SQLite can handle enough concurrency and operations, MySQL/MariaDB is probably an unnecessary layer
- [x] Framework
  - Needs CRUD-operations, Auth, Storage-layer connectivity, mail-integration
  - [Symfony 5](https://symfony.com/5) with [EasyAdmin](https://symfony.com/doc/current/bundles/EasyAdminBundle)
  - Tested, serious alternatives:
    - [Laravel Auth](https://github.com/jeremykenedy/laravel-auth): Pre-packaged, forced integration
    - [ORY Kratos](https://www.ory.sh/kratos/): API-based user-management and Auth
    - [UserFrosting](https://www.userfrosting.com/): More classical, but slower and more integrated
    - [Laravel Passport](https://laravel.com/docs/8.x/passport): Same level, but forces integration
    - [Laravel](https://laravel.com/), [Lumen](https://lumen.laravel.com/): Needs much implementation
    - Evaluated and considered some 20+ more, they're all incompatible with latest tech, standards, or unmaintained

## Services

- Portal at 80 (HTTP, redirecting) and 443 (HTTPS, forced)
- SSH at port 2244
- MySQL (MariaDB) via SSH at port 2244
- Prometheus at port 9090
- _Grafana at port 3000_
- CloudFlare for CDN, caching, HTTP/2/3

### RCPD: Raccoon City Police Department

See [RCPD 3.0](https://gitlab.com/swataftermath/rcpd)-repository.

### Wiki

A modern, JS-based workflow with low-level feature-parity for MediaWiki.

- [ ] VuePress
- [ ] MKDocs
- [ ] BoltCMS

### Tools

Each is a self-contained, Single Page Application.

- [ ] [MapMarker](https://gitlab.com/swataftermath/mapmarker)
  - v2 API runs independently wherever hosted
  - v3 API is remotely hosted
- [ ] Intelligence calculator
- [ ] Medal calculator

### Monitoring

Prometheus-metrics endpoint and internal Grafana dashboard. Alternative:

- [Real-time monitoring](https://goaccess.io/): https://hub.docker.com/r/allinurl/goaccess/

## Usage

- Start: `docker-compose up --detach`
- Start: `docker-compose down`
- Rebuild: `docker-compose up --detach --remove-orphans --force-recreate --always-recreate-deps --build`
- Monitor with CTop: `docker attach monitor`

### Add SSH-users

Requires key-pair authentication and declared users.

1. Edit `./docker/sshd/Dockerfile`
2. Replicate user-setup for [each additional user](https://wiki.alpinelinux.org/wiki/Setting_up_a_new_user#adduser)
3. Edit `./docker/sshd/sshd_config`
4. Add usernames to `AllowUsers`, space-separated

### Connecting to MySQL

1. In a terminal, run `plink.exe -ssh user@server -P 2244 -i "key.ppk" -N -L 3308:localhost:3306`
  - `user` is a SSH-user added to the SSHD-service
  - `server` is a domain or IP-address that will resolve to the Docker-host
  - `"key.ppk"` is a PuTTYgen-compatible private key
2. Enter your passphrase to unlock the key, and press Enter when prompted by "Access granted. Press Return to begin session."
3. Connect through a database manager such as HeidiSQL, connecting to `localhost` on port `3306` with the username `swat` and password `hguyFt6S95dgfR4ryb`
